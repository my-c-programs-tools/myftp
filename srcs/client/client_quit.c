#include "client.h"

int		    quit(t_client *client, char **command)
{
  t_packet	packet;
  
  packet.type = CMD_QUIT;
  if (write(client->socket_fd, &packet, sizeof(packet)) == -1)
    return (-1);
  assert(client != NULL);
  assert(command != NULL);
  puts("Quit !");
  return (-1);
}

