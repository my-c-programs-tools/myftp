#include "client.h"

int		    help(t_client *client, char **command)
{
    UNUSED(client);
    UNUSED(command);
    puts("Available commands are :\ncd\nget [filename]\nls\nput [filename]\npwd\nquit");
    return (0);
}