##
## Makefile for ftp in /home/roman/Documents/dev/PSU_2014_myftp
## 
## Made by grout_r
## Login   <roman@epitech.net>
## 
## Started on  Thu Mar 19 13:49:44 2015 grout_r
## Last update Sat May  2 16:45:47 2020 VEROVE Jordan
##

SERVER_SRC_DIR =   srcs/server/

CLIENT_SRC_DIR =   srcs/client/

CC =		gcc

RM =		rm -f

INCLUDES_CLI = -Iincludes/client -I.

INCLUDES_SRV = -Iincludes/server -I.


CFLAGS =	-Wall -Werror -Wextra $(INCLUDES_SRV) $(INCLUDES_CLI)

LDFLAGS =	

SRC_SRV =	$(SERVER_SRC_DIR)server.c \
		    $(SERVER_SRC_DIR)server_init.c \
		    $(SERVER_SRC_DIR)server_core.c \
		    $(SERVER_SRC_DIR)server_treat.c \
            $(SERVER_SRC_DIR)server_put.c \
		    $(SERVER_SRC_DIR)server_get.c \
		    $(SERVER_SRC_DIR)server_ls.c \
		    $(SERVER_SRC_DIR)server_pwd.c \
		    $(SERVER_SRC_DIR)server_cd.c \
		    $(SERVER_SRC_DIR)server_user.c \

SRC_CLI = 	$(CLIENT_SRC_DIR)client.c \
            $(CLIENT_SRC_DIR)client_init.c \
            $(CLIENT_SRC_DIR)client_core.c \
            $(CLIENT_SRC_DIR)client_command.c \
            $(CLIENT_SRC_DIR)client_execute.c \
            $(CLIENT_SRC_DIR)client_put.c \
            $(CLIENT_SRC_DIR)client_get.c \
            $(CLIENT_SRC_DIR)client_ls.c \
            $(CLIENT_SRC_DIR)client_pwd.c \
            $(CLIENT_SRC_DIR)client_cd.c \
            $(CLIENT_SRC_DIR)client_user.c \
            $(CLIENT_SRC_DIR)client_quit.c \
            $(CLIENT_SRC_DIR)client_help.c

OBJ_SRV =	$(SRC_SRV:.c=.o)

OBJ_CLI = 	$(SRC_CLI:.c=.o)

NAME_SRV =	./bin/server/server

NAME_CLI = 	./bin/client/client

all:		$(NAME_CLI) $(NAME_SRV)

$(NAME_CLI):	$(OBJ_CLI)
		$(CC) $(LDFLAGS) $(INCLUDES_CLI) $(OBJ_CLI) -o $(NAME_CLI)

$(NAME_SRV):	$(OBJ_SRV)
		$(CC) $(LDFLAGS) $(INCLUDES_SRV) $(OBJ_SRV) -o $(NAME_SRV)

clean:
		$(RM) $(OBJ_SRV) $(OBJ_CLI)

fclean:		clean
		$(RM) $(NAME_SRV) $(NAME_CLI)

re: fclean all

.PHONY: re all clean fclean
